imod.wq  -  Create Water Quality model
--------------------------------------

.. automodule:: imod.wq
    :members:
    :imported-members:
    :undoc-members:
    :show-inheritance:

.. automodule:: imod.wq.model
    :members: Model
    :undoc-members:
    :show-inheritance:

.. automodule:: imod.wq.pkgbase
    :members:
    :show-inheritance:
