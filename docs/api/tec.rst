imod.tec  -  Read Tecplot ASCII data
------------------------------------

.. automodule:: imod.tec
    :members:
    :undoc-members:
    :show-inheritance:
