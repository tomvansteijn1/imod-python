Regridding
==========

Since regridding or resampling is a common task while setting up a groundwater model, we provide convenience functions that make this as simple as possible.
